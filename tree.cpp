﻿#include <iostream>
using namespace std;


struct Node {
	int key;
	Node* left;
	Node* right;
	int height;
	Node(const int& k) :key(k), left(nullptr), right(nullptr), height(1) {}
};

class ComparatorDefault {
public:
	bool operator()(int first, int second) {
		return first < second;
	}
	
};

template<class T, class Comparator>
class AVLTree {
private:
	Node* root;
	Comparator cmp;
public:
	
	void Add(const T& key);
	void Del(const T& key);
	bool Exist(const T& key);
private:
	void delNode(Node& node);
	void addNode(Node *& node, const T& key);
	Node* fixBalance(Node*& node);
	void fixHeight(Node* node);
	int height(Node* node);
	int getCurrentBalance(Node* node);
	void rotateRight(Node*& node);
	void rotateLeft(Node*& node);
	void smallRotateRight(Node*& node);
	void smallRotateLeft(Node*& node);
	void bigRotateLeft(Node*& node);
	void bigRotateRight(Node*& node);
	bool exist(Node* node, const T& key);

};

template<class T, class Comparator>
void AVLTree<T, Comparator>::rotateLeft(Node*& node) {
	if(!node)
		return ;
	if (getCurrentBalance(node->right) == -1)
		bigRotateLeft(node);
	else 
		smallRotateLeft(node);
}

template<class T, class Comparator>
void AVLTree<T, Comparator>::bigRotateRight(Node*& node) {
	smallRotateLeft(node->left);
	smallRotateRight(node);
}

template<class T, class Comparator>
void AVLTree<T, Comparator>::bigRotateLeft(Node*& node) {
	smallRotateRight(node->right);
	smallRotateLeft(node);
}

template<class T, class Comparator>
void AVLTree<T, Comparator>::smallRotateLeft(Node*& node) {
	Node* rightChild = node->right;
	node->right = rightChild->left;
	rightChild->left = node;
	fixHeight(node);
	fixHeight(rightChild);
	node = rightChild;
}



template<class T, class Comparator>
void AVLTree<T, Comparator>::smallRotateRight(Node*& node) {
	Node* leftchild = node->left;
	node->left = leftchild->right;
	leftchild->right = node;
	fixHeight(node);
	fixHeight(leftchild);
	node = leftchild;
}

template<class T, class Comparator>
void AVLTree<T, Comparator>::rotateRight(Node*& node) {
	if (!node) {
		return;
	}
	if (getCurrentBalance(node->left) == -1) {
		smallRotateRight(node);
	}
	else {
		bigRotateRight(node);
	}
}

template<class T, class Comparator>
int AVLTree<T, Comparator>::getCurrentBalance(Node* node) {
	if (!node) {
		return 0;
	}
	return height(node->right) - height(node->left);
}

template<class T, class Comparator>
int AVLTree<T, Comparator>::height(Node* node) {
	if (!node) {
		return 0;
	}
	return node->height;
}

template<class T, class Comparator>
void AVLTree<T, Comparator>::fixHeight(Node* node) {
	if (!node)
		return;
	int left = height(node->left);
	int right = height(node->right);

	node->height = left > right ? left + 1 : right + 1;
}

template<class T, class Comparator>
Node* AVLTree<T, Comparator>::fixBalance(Node*& node) {
	fixHeight(node);
	if (getCurrentBalance(node) == -2) {
		rotateRight(node);
	}
	if (getCurrentBalance(node) == 2) {
		rotateLeft(node);
	}
	return node;
}

template<class T, class Comparator>
void AVLTree<T, Comparator>::addNode(Node*& node, const T& key) {
	if (!node) {
		node = new Node(key);
		return;
	}
	if (cmp(key,node->key)) {
		addNode(node->left, key);
	}
	else {
		addNode(node->right, key);
	}
	fixBalance(node);
}

template<class T, class Comparator>
bool AVLTree<T, Comparator>::Exist(const T& key) {
	return exist(root, key);
}

template<class T, class Comparator>
bool AVLTree<T, Comparator>::exist(Node* node, const T& key) {
	if (!node)
		return false;

	bool isExist;
	if (cmp(node->key, key)) {
		isExist = exist(node->right, key);
	}
	else if (cmp(key, node->key))
		isExist = exist(node->left, key);
	else
		isExist = true;
	return isExist;
}

template<class T, class Comparator>
void AVLTree<T, Comparator>::Add(const T& key) {
	addNode(root, key);
}

int main()
{
	AVLTree<int, ComparatorDefault> Tree;
	Tree.Add(5);
	Tree.Add(10);
	Tree.Add(15);
	Tree.Add(20);
	Tree.Add(25);
	Tree.Add(35);
	Tree.Exist(15);
	Tree.Exist(75);

}
